import { PREFIX } from '~/utils/configs';

const COMPONENT_CLASS = PREFIX + 'tab-panel';

export default {
  props: {
    title: String
  },

  data() {
    return {
      isActive: false
    };
  },

  render(h) {
    return h(
      'div',
      {
        attrs: {
          role: 'tabpanel'
        },
        staticClass: COMPONENT_CLASS,
        class: { active: this.isActive }
      },
      [this.$slots.default]
    );
  }
};
