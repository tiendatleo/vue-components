import { PREFIX } from "~/utils/configs";
import { COMPONENT_SIZE_LIST } from "~/utils/constants";

const COMPONENT_CLASS = PREFIX + "pagination";

const makePrevButton = (h, vm) => {
  const disabled = vm.current === 1;
  const { prevText, handlePrev } = vm;

  return vm.$scopedSlots.prevButton
    ? vm.$scopedSlots.prevButton({ disabled, prevText, handlePrev })
    : h(
        "li",
        {
          class: [`${COMPONENT_CLASS}__item`],
        },
        [
          h(
            "button",
            {
              attrs: {
                disabled,
                title: prevText,
              },
              staticClass: `${COMPONENT_CLASS}__item-link`,
              on: {
                click: handlePrev,
              },
            },
            prevText
          ),
        ]
      );
};

const makeNextButton = (h, vm) => {
  const disabled = vm.current >= vm.totalPages;
  const { nextText, handleNext } = vm;

  return vm.$scopedSlots.prevButton
    ? vm.$scopedSlots.prevButton({ disabled, nextText, handleNext })
    : h(
        "li",
        {
          class: [`${COMPONENT_CLASS}__item`],
        },
        [
          h(
            "button",
            {
              attrs: {
                disabled,
                title: nextText,
              },
              staticClass: `${COMPONENT_CLASS}__item-link`,
              on: { click: handleNext },
            },
            nextText
          ),
        ]
      );
};

export default {
  name: COMPONENT_CLASS,

  model: {
    prop: "current",
    event: "change",
  },

  props: {
    tag: {
      type: String,
      default: "nav", // 'button', 'a'
    },
    size: {
      type: String,
      default: COMPONENT_SIZE_LIST[0],
      validator: (value) => {
        if (COMPONENT_SIZE_LIST.includes(value)) {
          return true;
        } else {
          printWarning(
            `${PREFIX}pagination prop 'size' must be in ${JSON.stringify(
              COMPONENT_SIZE_LIST
            )}, current: '${value}'`
          );
          return false;
        }
      },
    },
    current: {
      type: Number,
      required: true,
      default: 1,
    },
    pageSize: {
      type: Number,
      required: true,
    },
    total: {
      type: Number,
      required: true,
    },
    totalText: {
      type: Function,
      default: () => "",
    },
    hideOnSinglePage: Boolean,
    maxVisibleButtons: {
      type: Number,
      default: 5,
    },
    prevText: {
      type: String,
      default: "Prev",
    },
    nextText: {
      type: String,
      default: "Next",
    },
    listWrapperClass: {
      type: [Object, Array, String],
      default: () => ({}),
    },
  },

  computed: {
    totalPages() {
      return this.pageSize ? Math.ceil(this.total / this.pageSize) : 0;
    },

    // https://css-tricks.com/creating-a-reusable-pagination-component-in-vue/
    paginationTriggers() {
      const { current, maxVisibleButtons, totalPages } = this;
      const visiblePagesThreshold = Math.floor((maxVisibleButtons - 1) / 2);
      const pagintationTriggersArray = Array(
        (totalPages < maxVisibleButtons ? totalPages : maxVisibleButtons) - 1
      ).fill(0);

      if (current <= visiblePagesThreshold + 1) {
        pagintationTriggersArray[0] = 1;
        const pagintationTriggers = pagintationTriggersArray.map(
          (paginationTrigger, index) => {
            return pagintationTriggersArray[0] + index;
          }
        );

        if (totalPages > 1 && totalPages < maxVisibleButtons) {
          pagintationTriggers.push(totalPages);
        } else if (totalPages >= maxVisibleButtons) {
          pagintationTriggers.push(0, totalPages);
        }

        return pagintationTriggers;
      }

      if (current >= totalPages - visiblePagesThreshold) {
        const pagintationTriggers = pagintationTriggersArray.map(
          (paginationTrigger, index) => {
            return totalPages - index;
          }
        );
        pagintationTriggers.reverse().unshift(1, -1);
        return pagintationTriggers;
      }

      pagintationTriggersArray[0] = current - visiblePagesThreshold + 1;
      const pagintationTriggers = pagintationTriggersArray.map(
        (paginationTrigger, index) => {
          return pagintationTriggersArray[0] + index - 1;
        }
      );
      pagintationTriggers.unshift(1, -1);
      pagintationTriggers.pop();
      pagintationTriggers.push(0, totalPages);
      return pagintationTriggers;
    },
  },

  methods: {
    handlePrev(e) {
      e.preventDefault();
      this.$emit("change", this.current - 1);
    },

    handleNext(e) {
      e.preventDefault();
      this.$emit("change", this.current + 1);
    },

    handleChange(page) {
      if (page > 0) this.$emit("change", page);
    },
  },

  render: function(h) {
    const prevButton = makePrevButton(h, this);
    const nextButton = makeNextButton(h, this);

    if (this.hideOnSinglePage && this.totalPages <= 1) return h();

    return h(
      this.tag,
      {
        staticClass: COMPONENT_CLASS,
        class: [`${COMPONENT_CLASS}--${this.size}`],
      },
      [
        this.totalText || this.$scopedSlots.totalText
          ? h(
              "span",
              { staticClass: `${COMPONENT_CLASS}__total-text` },
              this.$scopedSlots.totalText
                ? this.$scopedSlots.totalText({
                    current: this.current,
                    total: this.total,
                    pageSize: this.pageSize,
                  })
                : this.totalText(this.current, this.total, this.pageSize)
            )
          : null,
        h(
          "ul",
          {
            staticClass: `${COMPONENT_CLASS}__ul`,
            class: this.listWrapperClass,
          },
          [
            prevButton,
            ...this.paginationTriggers.map((page) => {
              return h(
                "li",
                {
                  key: page,
                  class: [
                    `${COMPONENT_CLASS}__item`,
                    {
                      [`${COMPONENT_CLASS}__item--current`]:
                        page === this.current,
                      [`${COMPONENT_CLASS}__item--dumb`]: page <= 0,
                    },
                  ],
                },
                [
                  this.$scopedSlots.itemRender
                    ? this.$scopedSlots.itemRender({
                        page,
                        text: page <= 0 ? "•••" : page,
                        isDumb: page <= 0,
                        linkClass: `${COMPONENT_CLASS}__item-link`,
                        handleClick: this.handleChange,
                      })
                    : h(
                        "button",
                        {
                          attrs: {
                            title: "",
                          },
                          staticClass: `${COMPONENT_CLASS}__item-link`,
                          on: {
                            click: () => this.handleChange(page),
                          },
                        },
                        page <= 0 ? "•••" : page
                      ),
                ]
              );
            }),
            nextButton,
          ]
        ),
      ]
    );
  },
};
