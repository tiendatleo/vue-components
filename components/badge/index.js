import { PREFIX } from '~/utils/configs';
import { THEME_VARIANT_LIST } from '~/utils/constants';
import { printWarning } from '~/utils/handle-error';

const COMPONENT_CLASS = PREFIX + 'badge';

const classes = props => {
  const variantClasses = THEME_VARIANT_LIST.reduce((cls, variant) => {
    if (!variant) return cls;
    cls[`${COMPONENT_CLASS}--${variant}`] =
      props.variant === variant ? true : false;
    return cls;
  }, {});

  return {
    ...variantClasses
  };
};

export default {
  name: `${PREFIX}badge`,

  props: {
    tag: {
      type: String,
      default: 'div'
    },
    variant: {
      type: String,
      default: THEME_VARIANT_LIST[0],
      validator: value => {
        if (THEME_VARIANT_LIST.includes(value)) {
          return true;
        } else {
          printWarning(
            `${PREFIX}button prop 'variant' must be in ${JSON.stringify(
              THEME_VARIANT_LIST
            )}, current: '${value}'`
          );
          return false;
        }
      }
    },
    color: {
      type: String,
      default: ''
    },
    count: {
      type: [Number, String],
      default: ''
    },
    overflowCount: {
      type: Number,
      default: 99
    },
    showZero: Boolean,
    dot: Boolean
  },

  render: function(h) {
    const badge = this.dot
      ? h('sup', {
          class: [`${COMPONENT_CLASS}__dot`]
        })
      : !this.showZero && this.count == 0
      ? null
      : h(
          'sup',
          {
            class: [`${COMPONENT_CLASS}__count`]
          },
          [this.count > this.overflowCount ? `${this.overflowCount}+` : this.count]
        );

    return h(
      this.tag,
      {
        attrs: this.$attrs,
        class: [
          COMPONENT_CLASS,
          classes(this.$props),
          this.staticClass,
          this.class
        ],
        on: this.$listeners
      },
      [this.$slots.default, badge]
    );
  }
};
