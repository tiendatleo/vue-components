const w = typeof window !== 'undefined' ? window : {};

/**
 * Use window.requestAnimationFrame() method.
 * If requestAnimationFrame not supported,
 * run a callback timeout with 16ms instead athough it's not really true.
 */
export const requestAF =
  w.requestAnimationFrame ||
  w.webkitRequestAnimationFrame ||
  w.mozRequestAnimationFrame ||
  w.msRequestAnimationFrame ||
  w.oRequestAnimationFrame ||
  // Fallback, but not a true polyfill
  // Only needed for Opera Mini
  /* istanbul ignore next */
  (cb => setTimeout(cb, 16));

/**
 * To get offset of element
 * @param { HTMLElement } el
 */
export const offset = el => {
  let left = 0;
  let top = 0;

  if (el.offsetParent) {
    do {
      left += el.offsetLeft;
      top += el.offsetTop;
    } while ((el = el.offsetParent));
  }

  return {
    left,
    top
  };
};

/**
 * To check event is touch event
 * @param {Event} event
 */
export const checkTouchEvent = event => event instanceof TouchEvent;
