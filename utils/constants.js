export const THEME_VARIANT_LIST = [
  'primary',
  'secondary',
  'success',
  'danger',
  'warning',
  'info',
  'dark',
  'light'
];

export const COMPONENT_SIZE_LIST = ['md', 'sm', 'lg'];
