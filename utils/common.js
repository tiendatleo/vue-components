//
export const isBrowser =
  typeof window !== 'undefined' && typeof document !== undefined;

/**
 * To set size of vue component.
 * @param {[String, Number]} size
 */
export function setPropSize(size) {
  if (typeof size === 'number') {
    return size + 'px';
  }

  return size || null;
}

/**
 * To get key code of key event then run callback
 */
export function dispatchForCode(event, callback) {
  let code;
  console.log('event', event.key, event.keyCode, event.code);

  if (event.key !== undefined) {
    code = event.key;
  } else if (event.keyCode !== undefined) {
    code = event.keyCode;
  } else {
    code = event.code;
  }

  callback(code);
}
