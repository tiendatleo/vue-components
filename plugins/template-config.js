import { PREFIX } from '~/utils/configs';

export default (ctx, inject) => {
  const config = {
    prefix: PREFIX
  };

  inject('templateConfig', config);
};
